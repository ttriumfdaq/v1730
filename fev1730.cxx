/********************************************************************\

This is a MIDAS front-end for reading DPP data from CAEN 
V1730 through VME.

This version does not yet work!

Author: T. Lindner
Data: Feb 2015

  $Id$
\********************************************************************/
#define CAEN_VME
#define V1730_CODE

#include <stdio.h>
#include <stdlib.h>
#include "midas.h"
#include "mvmestd.h"
#include "gefvme.h"
#include "mfe.h"

extern "C" {
#ifdef CAEN_VME
#endif
#include "v1730drv.h"
}

#include "OdbV1730.h"
//#include "feveto.h"

#define  EQ_NAME   "FEV1730"
#define  EQ_EVID   1
#define  EQ_TRGMSK 0x0100

/* Globals */
#define N_V1730 1

/* Hardware */
MVME_INTERFACE *myvme;
uint32_t V1730_BASE[N_V1730] = {0x32300000};
extern HNDLE hDB;
extern BOOL debug;

HNDLE hSet[N_V1730];
V1730_CONFIG_SETTINGS tsvc[N_V1730];
const char BankName[N_V1730][5]={"V730"};

// VMEIO definition



/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
const char *frontend_name = "feV1730";
/* The frontend file name, don't change it */
const char *frontend_file_name = (char*)__FILE__;

BOOL equipment_common_overwrite = FALSE;

/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 000;

/* maximum event size produced by this frontend */
INT max_event_size = 32 * 34000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 2 * max_event_size + 10000;

/* VME base address */
int   v1730_handle[N_V1730];

int  linRun = 0;
int  done=0, stop_req=0;

/*-- Function declarations -----------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();
extern void interrupt_routine(void);
INT read_trigger_event(char *pevent, INT off);
INT read_buffer_level(char *pevent, INT off);

/*-- Equipment list ------------------------------------------------*/
#undef USE_INT
EQUIPMENT equipment[] = {

  { EQ_NAME,                 /* equipment name */
    {
      EQ_EVID, EQ_TRGMSK,     /* event ID, trigger mask */
      "SYSTEM",              /* event buffer */
      EQ_POLLED ,      /* equipment type */
      LAM_SOURCE(0, 0x0),     /* event source crate 0, all stations */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING,             /* read only when running */
      500,                    /* poll for 500ms */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",
    },
    read_trigger_event,       /* readout routine */
  },

	{                                                                                                                   
		"BUFLVLV1730",                /* equipment name */                                                           
		{                                                                                                               
			105, 0,                   /* event ID, trigger mask */                                                      
			"",               /* event buffer */                                                                  
			EQ_PERIODIC,   /* equipment type */                                                                         
			0,                      /* event source */                                                                  
			"MIDAS",                /* format */                                                                        
			TRUE,                   /* enabled */                                                                       
			RO_RUNNING | RO_TRANSITIONS |   /* read when running and on transitions */                                  
			RO_ODB,                 /* and update ODB */                                                                
			1000,                  /* read every 1 sec */                                                               
			0,                      /* stop run after this event limit */                                               
			0,                      /* number of sub events */                                                          
			1,                      /* log history */                                                                   
			"", "", ""                                                                                                  
		},                                                                                                              
		read_buffer_level,       /* readout routine */                                                                  
	},                                                                                                                  
  {""}
};



/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/********************************************************************/

/*-- Sequencer callback info  --------------------------------------*/
void seq_callback(INT hDB, INT hseq, void *info)
{
  KEY key;

	printf("odb ... Settings %x touched\n", hseq);
  for (int b=0;b<N_V1730;b++) {
    if (hseq == hSet[b]) {
      db_get_key(hDB, hseq, &key);
      printf("odb ... Settings %s touched\n", key.name);
    }
  }
}

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{
  int size, status;
  char set_str[80];

  // Suppress watchdog for PICe for now
  cm_set_watchdog_params(FALSE, 0);

  //  setbuf(stdout, NULL);
  //  setbuf(stderr, NULL);
  printf("begin of Init\n");
  /* Book Setting space */
  V1730_CONFIG_SETTINGS_STR(v1730_config_settings_str);

  sprintf(set_str, "/Equipment/feV1730/Settings/V1730");
  status = db_create_record(hDB, 0, set_str, strcomb(v1730_config_settings_str));
  status = db_find_key (hDB, 0, set_str, &hSet[0]);
  if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);

  /* Enable hot-link on settings/ of the equipment */
  size = sizeof(V1730_CONFIG_SETTINGS);
  if ((status = db_open_record(hDB, hSet[0], &(tsvc[0]), size, MODE_READ, seq_callback, NULL)) != DB_SUCCESS)
    return status;

#ifdef CAEN_VME
  // Open VME interface   
  status = mvme_open(&myvme, 0);
#endif


	//--------------- Init cm_msg debug -----------------------

  for (int module=0;module<N_V1730;module++) {

		//int status = v1730_RegisterRead(myvme, V1730_BASE[module], V1730_VME_STATUS);
		//printf("VME status: %i\n",status);
    v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_SW_RESET, 1);
		//    v1730_AcqCtl(myvme, V1730_BASE[module], V1730_SW_CLEAR);
  }




  char ss_fw_datatype[256]= {"Module "};
	char str[64];
  // Firmware version check (0x1n8C)
  // read each AMC firmware version
  // [31:16] Revision date Y/M/DD
  // [15:8] Firmware Revision (X)
  // [7:0] Firmware Revision (Y)
  // eg 0x760C0103 is 12th June 07, revision 1.3
  int addr = 0;
  uint32_t version = 0;
  uint32_t prev_chan = 0;
  // Hardcode correct firmware verisons
  // Current release 3.4_0.11 Feb 2012
  // AMC FW: 0xd3280005, ROC FW: 0xd4110401
  const uint32_t amc_fw_ver = 0xe7028802; // Update 27Nov2014
  const uint32_t roc_fw_ver = 0xe4280402; // Update 27Nov2014

	// Somehow this doesn't work if we don't read the register first once
	version = v1730_RegisterRead(myvme,  V1730_BASE[0], 0x108c);
  for(int iCh=0;iCh<8;iCh++) {
    addr = 0x108c | (iCh << 8);
		version = v1730_RegisterRead(myvme,  V1730_BASE[0], addr);
				//printf("%i %x %x \n",iCh, addr,version);
			if((iCh != 0) && (prev_chan != version)) {
		 cm_msg(MERROR, "Initialize","Error Channels have different AMC Firmware ");
			}
    prev_chan = version;
  }
	if(version != amc_fw_ver) {
		cm_msg(MERROR,"Initialize","Incorrect AMC Firmware Version: 0x%08x", version);
	} else {
		sprintf(str, "AMC FW: 0x%x, ", version);
    strcat(ss_fw_datatype, str);
	}
	printf("AMC Firmware revision date: 20%i/%i/%i\n",
				 (version & 0xf0000000) >> 28,
				 (version & 0x0f000000) >> 24,
				 (version & 0x00ff0000) >> 16);

  // read ROC firmware revision
  // Format as above
	version = v1730_RegisterRead(myvme,  V1730_BASE[0], V1730_ROC_FPGA_FW_REV);
	switch (version) {
	case roc_fw_ver:
		sprintf(str, "ROC FW: 0x%x", version);
		strcat(ss_fw_datatype, str);
	 break;
	default:
		cm_msg(MERROR,"Initialize","Incorrect ROC Firmware Version: 0x%08x", version);
		break;
	}
	printf("ROC Firmware revision date: 20%i/%i/%i\n",
				 (version & 0xf0000000) >> 28,
				 (version & 0x0f000000) >> 24,
				 (version & 0x00ff0000) >> 16);

	INT reg  = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_ACQUISITION_STATUS);
	sprintf(str, ", Acq Reg: 0x%x", reg); 
	strcat(ss_fw_datatype, str);
  cm_msg(MINFO, "Initialize", ss_fw_datatype);

  // Stop board
  for (int module=0;module<N_V1730;module++) {
    v1730_AcqCtl(myvme, V1730_BASE[module], V1730_RUN_STOP);
  }

	//--------------- End of Init cm_msg debug ----------------

	set_equipment_status(equipment[0].name, "Initialized", "#00ff00");

	//exit(0);
  printf("end of Init\n");
  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{
  printf("End of exit\n");
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{

#ifdef CAEN_VME
#ifdef V1730_CODE
  char stastr[64];
  int status;
  uint32_t temp;
  //-------- V1730 (8ch 500Msps 124bits CAEN board) ----------------------------------
  printf("BOR\n");

  // Stop board
  for (int module=0;module<N_V1730;module++) {
    v1730_AcqCtl(myvme, V1730_BASE[module], V1730_RUN_STOP);
  }

  /* read Triggger settings */
	int nInitOk=0;
  for (int module=0; module<N_V1730; module++) {
    int size = sizeof(V1730_CONFIG_SETTINGS);
    if ((status = db_get_record (hDB, hSet[module], &tsvc[module], &size, 0)) != DB_SUCCESS)
      return status;

    // load setup config is != 0
    if (tsvc[module].setup != 0) {
      v1730_Setup(myvme, V1730_BASE[module], tsvc[module].setup);
    } 
    { 
      printf(" setup \n");
      // Reset ACQ Control
      v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_ACQUISITION_CONTROL, 0x00);
      // Acq mode 3:Reg 
      v1730_AcqCtl(myvme, V1730_BASE[module], tsvc[module].acq_mode);


      v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_BUFFER_ORGANIZATION , tsvc[module].buffer_organization);
      // 0=disabled using BUFFER_ORGANIZATION
      v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_CUSTOM_SIZE         , tsvc[module].custom_size);

      // channel enable 
			printf("Setting channel mask 0x%x\n",tsvc[module].ch_mask);
			v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_CHANNEL_EN_MASK       , tsvc[module].ch_mask);
			int mask = v1730_RegisterRead(myvme, V1730_BASE[module], V1730_CHANNEL_EN_MASK);
			printf("Channel mask readback 0x%x\n",mask);

			
      //      v1730_AcqCtl(myvme, V1730_BASE[module], V1730_COUNT_ACCEPTED_TRIGGER);
      
			// Channel issuing trigger
      v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_TRIG_SRCE_EN_MASK     , tsvc[module].trigger_source);
      int readback_mask = v1730_RegisterRead(myvme, V1730_BASE[module], V1730_TRIG_SRCE_EN_MASK );
			printf("Readback channel mask 0x%x\n",readback_mask);

      // channel on ECL OUT for trigger
      v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_FP_TRIGGER_OUT_EN_MASK, tsvc[module].trigger_output);

      v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_POST_TRIGGER_SETTING  , tsvc[module].post_trigger);

      v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_ALMOST_FULL_LEVEL , tsvc[module].almost_full);


      // Buff occupancy on MON out
      v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_MONITOR_MODE          , 0x3);  // buffer occupancy


      v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_VME_CONTROL, V1730_ALIGN64);

			// Check the channel configuration
			v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_CHANNEL_CONFIG,0x10010); 
			int config = v1730_RegisterRead(myvme, V1730_BASE[module], V1730_CHANNEL_CONFIG); 
			printf("Channel config = 0x%x\n",config);


			v1730_RegisterWrite(myvme, V1730_BASE[module], 0x8020, tsvc[module].sample_length); 
			int nsamples = v1730_RegisterRead(myvme, V1730_BASE[module], 0x8020); 
			printf("Nsamples = 0x%x\n",nsamples);
			//			0x8020

			// Setup Busy daisy chaining
      v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_FP_IO_CONTROL,   0x104); // 0x100:enable new config, 0x4:LVDS I/O[3..0] output
			v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_FP_LVDS_IO_CRTL, 0x022); // 0x20: I/O[7..4] input mode nBusy/Veto (4:nBusy input)
                                                                                    // 0x02: I/O[3..0] output mode nBusy/Veto (1:Busy)
			int addr;
      // Set channel threshold
      for (int i=0;i<8;i++) {
				addr = V1730_CHANNEL_THRESHOLD | (i << 8);
        v1730_RegisterWrite(myvme, V1730_BASE[module], addr , tsvc[module].threshold[i]);
				temp = v1730_RegisterRead(myvme, V1730_BASE[module], addr);
        printf("Board: %d Threshold[%i] = %d \n", module, i, temp);
      }
			
      // Set DAC value
      for (int i=0;i<8;i++) {
				addr = V1730_CHANNEL_DAC | (i << 8);
        v1730_RegisterWrite(myvme, V1730_BASE[module], addr , tsvc[module].dac[i]);
				temp = v1730_RegisterRead(myvme, V1730_BASE[module], addr);
        printf("Board: %d DAC[%i] = %d \n", module, i, temp);
      }
      

      // Set Board ID in the data stream!
      v1730_RegisterWrite(myvme, V1730_BASE[module], V1730_BOARD_ID, 6);
      printf(" setup \n");

      // print status
      v1730_Status(myvme, V1730_BASE[module]);

			// Abort if PLL not locked properly
			INT reg  = v1730_RegisterRead(myvme, V1730_BASE[module], V1730_ACQUISITION_STATUS);
			cm_msg(MINFO, "AcqInit", "Acquisition Status : 0x%x", reg);
			if (!(reg & 0x80)) nInitOk++;
    } // setup != 0
  } // loop over module

	// Abort if PLL not locked properly
	if (nInitOk) {
		cm_msg(MINFO, "AcqInit", "PLL issues (%d)", nInitOk);
		return FE_ERR_HW;
	}

  // Start boards
  for (int module=0;module<N_V1730;module++) {
    if (V1730_BASE[module] < 0) continue;   // Skip unconnected board
    // Start run then wait for trigger
    v1730_AcqCtl(myvme, V1730_BASE[module], V1730_RUN_START);
  }
#endif
#endif
 
  linRun = 1;
  //------ FINAL ACTIONS before BOR -----------
  printf("End of BOR\n");
  //sprintf(stastr,"GrpEn:0x%x", tsvc[0].group_mask); 
  set_equipment_status("feVeto", stastr, "#00FF00");                                                                        
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{

  printf("EOR\n");
  // Stop run 
#ifdef CAEN_VME
  for (int module=0;module<N_V1730;module++) {
    v1730_AcqCtl(myvme, V1730_BASE[module], V1730_RUN_STOP);
  }
#endif
  // Stop DAQ for seting up the parameters
  done = 0;
  stop_req = 0;
  linRun = 0;

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  linRun = 0;
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  linRun = 1;
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{

  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  char str[128];
  static DWORD evlimit;

  if (stop_req && done==0) {
    db_set_value(hDB,0,"/logger/channels/0/Settings/Event limit", &evlimit, sizeof(evlimit), 1, TID_DWORD); 
    if (cm_transition(TR_STOP, 0, str, sizeof(str), BM_NO_WAIT, FALSE) != CM_SUCCESS) {
      cm_msg(MERROR, "feodeap", "cannot stop run: %s", str);
    }
    linRun = 0;
    done = 1;
    cm_msg(MERROR, "feodeap","feodeap Stop requested");
  }
  return SUCCESS;
}

/*------------------------------------------------------------------*/
/********************************************************************\
  Readout routines for different events
\********************************************************************/
int Nloop, Ncount;

/*-- Trigger event routines ----------------------------------------*/
INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  register int i;  // , mod=-1;
  register uint32_t lam;

  for (i = 0; i < count; i++) {
    //    if (v1730_handle[0] < 0) continue;   // Skip unconnected board
    //    if((i == 0) && !test)
    //      printf("count is %d\n", count);
    //    if((i % 50000 == 0) && !test)
    //      printf("polling %d\n", i);
    //    if (v1730_handle[0] < 0) continue;   // Skip unconnected board
    lam = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_EVENT_STORED);
		ss_sleep(1);
    //    lam = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_VME_STATUS);
    //lam = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_ACQUISITION_STATUS);
    //    if (test==0) printf("Handle:%d loop: %d lam:%x\n",v1730_handle[0], i, lam);
    //    lam &= 0x1;
    if (lam) {
      Nloop = i; Ncount = count;
      if (!test){
				//        printf("poll successfull, V1730_EVENT_STORED: 0x%x\n", lam);
        return lam;
      }
    }
  }
  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
 INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/
int vf48_error = 0;
INT read_trigger_event(char *pevent, INT off)
{
	printf("entered read trig\n");
	
  uint32_t  *pddata;
	uint32_t nEvtSze;
	//  int32_t  unEvtSze;

  uint32_t sn = SERIAL_NUMBER(pevent);

  // Skip if not seen BOR
  //  if (linRun == 0) return 0;


  // Create event header
  bk_init32(pevent);
	
  {
    // create bank header
    bk_create(pevent, BankName[0], TID_DWORD, (void**)&pddata);
		
    // Current event size
    nEvtSze = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_EVENT_SIZE);
    if (nEvtSze == 0) { printf("Error S/N:%d Bank Veto ", sn); }
		//		printf("Event size:%d\n", nEvtSze);
		
    // read event in DMA mode using KO V1742 example
		v1730_DataBlockRead(myvme, V1730_BASE[0], pddata, nEvtSze);
		pddata += nEvtSze;

#if 0
    unEvtSze = nEvtSze;
    do {
      n32read = v1730_DataBlockRead(myvme, V1730_BASE[0], pddata, nEvtSze);
      unEvtSze -= n32read;
      // adjust data pointer
      pddata += n32read;
            printf("nEvtSze:%d unEvtSze:%d n32read:%d \n", nEvtSze, unEvtSze, n32read);
    } while (unEvtSze > 0);

    // for statistics
    int nEventStored = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_EVENT_STORED);
    if (nEventStored != 0) {
      printf("S/N:%d Bank:VETO nEvtStored:%d  RetEvtSze_Arg:%d RetEvtSze:%d\n", sn, nEventStored, unEvtSze, n32read);
    }
#endif

    // Read event in loop mode
#if 0
    n32read = v1730_DataRead(myvme, V1730_BASE[0], pddata, nEvtSze);
    // for statistics
    int nEventStored = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_EVENT_STORED);

    printf("S/N:%d Bank:VETO nEvtStored:%d  ReqEvtSze:%d RetEvtSze:%d\n", sn, nEventStored, nEvtSze, n32read);

    // adjust data pointer
    pddata += n32read;
#endif

    bk_close(pevent, pddata);
  }

  //primitive progress bar
  if (sn % 100 == 0) printf(".");

  return bk_size(pevent);
}
  
//
//--------------------------------------------------------------------------------
INT read_buffer_level(char *pevent, INT off) {
	DWORD * pddata;
  bk_init32(pevent);
#if 0
	INT lam  = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_ACQUISITION_STATUS);
	INT lam1 = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_ACQUISITION_CONTROL);
	INT lam2 = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_BUFFER_ORGANIZATION);
	INT lam3 = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_TRIG_SRCE_EN_MASK);
	INT lam4 = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_ROC_FPGA_FW_REV);
 	INT lam5 = 0;//v1730_RegisterRead(myvme, V1730_BASE[0], V1730_GROUP_FW_REV);
	INT lam6 = v1730_RegisterRead(myvme, V1730_BASE[0], 0xEF20);
	cm_msg(MINFO, "debug", "0x8104:0x%x 0x8100:0x%x 0x800c:0x%x 0x810c:0x%x 0x8124:0x%x 0x108c:0x%x 0xEF20:0x%x"
				 , lam, lam1, lam2, lam3, lam4, lam5, lam6);
#endif
	bk_create(pevent, "BV30", TID_DWORD, (void**)&pddata);
	*pddata++ = v1730_RegisterRead(myvme, V1730_BASE[0], V1730_EVENT_STORED);
	bk_close(pevent, pddata);
  return bk_size(pevent);
}
