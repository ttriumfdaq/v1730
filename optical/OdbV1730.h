/***************************************************************************/
/*                                                                         */
/*  Filename: OdbV1730.h                                                   */
/*                                                                         */
/*  Function: headerfile for a single V1730 module                         */
/*                                                                         */
/* ----------------------------------------------------------------------- */
/*                                                                         */
/***************************************************************************/

#ifndef  ODBV1730_INCLUDE_H
#define  ODBV1730_INCLUDE_H

typedef struct {
  INT       setup;
  INT       acq_mode;               // 0x8100@[ 1.. 0]
  INT       buffer_organization;    // 0x800C@[ 3.. 0]
  INT       custom_size;            // 0x8020@[31.. 0]
  INT       ch_mask;                // 0x8120@[15.. 0]
  INT       sample_length;                // 0x8120@[15.. 0]
  DWORD     trigger_source;         // 0x810C@[31.. 0]
  DWORD     trigger_output;         // 0x8110@[31.. 0]
  DWORD     post_trigger;           // 0x8114@[31.. 0]
  DWORD     almost_full;            // 0x816C@[31.. 0]
  DWORD     threshold[16];                 // 0x1n80@[15.. 0]
  DWORD     dac[16];                 // 0x1n98@[15.. 0]
  DWORD     gain[16];                 // 0x1n28@[15.. 0]
} V1730_CONFIG_SETTINGS;

#define V1730_CONFIG_SETTINGS_STR(_name) const char *_name[] = {\
"setup = INT : 0",\
"Acq mode = INT : 3",\
"Buffer organization = INT : 0xa",\
"Custom size = INT : 0",\
"Channel Mask = DWORD : 65535",\
"Sample Length = DWORD : 255",\
"Trigger Source = DWORD : 0xC0000000",\
"Trigger Output = DWORD : 0xf",\
"Post Trigger = DWORD : 200",\
"Almost Full = DWORD : 512",\
"Threshold = DWORD[16] :",\
"[0] 0x0",\
"[1] 0x0",\
"[2] 0x0",\
"[3] 0x0",\
"[4] 0x0",\
"[5] 0x0",\
"[6] 0x0",\
"[7] 0x0",\
"[8] 0x0",\
"[9] 0x0",\
"[10] 0x0",\
"[11] 0x0",\
"[12] 0x0",\
"[13] 0x0",\
"[14] 0x0",\
"[15] 0x0",\
"DAC = DWORD[16] :",\
"[0] 0xEA60",\
"[1] 0xEA60",\
"[2] 0xEA60",\
"[3] 0xEA60",\
"[4] 0xEA60",\
"[5] 0xEA60",\
"[6] 0xEA60",\
"[7] 0xEA60",\
"[8] 0xEA60",\
"[9] 0xEA60",\
"[10] 0xEA60",\
"[11] 0xEA60",\
"[12] 0xEA60",\
"[13] 0xEA60",\
"[14] 0xEA60",\
"[15] 0xEA60",\
"GAIN = DWORD[16] :",\
"[0] 0x0",\
"[1] 0x0",\
"[2] 0x0",\
"[3] 0x0",\
"[4] 0x0",\
"[5] 0x0",\
"[6] 0x0",\
"[7] 0x0",\
"[8] 0x0",\
"[9] 0x0",\
"[10] 0x0",\
"[11] 0x0",\
"[12] 0x0",\
"[13] 0x0",\
"[14] 0x0",\
"[15] 0x0",\
NULL }
#endif  //  ODBV1730_INCLUDE_H
