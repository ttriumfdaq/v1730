/********************************************************************\
This is a MIDAS front-end for reading raw waveforms from CAEN 
V1730 through CONET2 optical interface.

Author: T. Lindner
Data: April 2015

  $Id$
\********************************************************************/
#define CAEN_VME
#define V1730_CODE

#include <stdio.h>
#include <stdlib.h>
#include "midas.h"
#include "mvmestd.h"
#include "gefvme.h"
#include "unistd.h"
#include <iostream>
#include "v1730Board.h"
#include "mfe.h"

extern "C" {
#include "CAENComm.h"
}

extern "C" {
#ifdef CAEN_VME
#endif
#include "v1730drv.h"
}

#include "OdbV1730.h"

#define  EQ_NAME   "FEV1730OPTIC"
#define  EQ_EVID   1
#define  EQ_TRGMSK 0x0100

/* Globals */
#define N_V1730 1



/* Hardware */
MVME_INTERFACE *myvme;
//uint32_t V1730_BASE[N_V1730] = {0x3230000};
uint32_t V1730_BASE[N_V1730] = {0x510000}; // try 24bit address
extern HNDLE hDB;
extern BOOL debug;

v1730Board *myv1730Board;

HNDLE hSet[N_V1730];
V1730_CONFIG_SETTINGS tsvc[N_V1730];
const char BankName[N_V1730][5]={"V730"};

// VMEIO definition

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
const char *frontend_name = "feV1730Raw";
/* The frontend file name, don't change it */
const char *frontend_file_name = (char*)__FILE__;

BOOL equipment_common_overwrite = FALSE;


/* frontend_loop is called periodically if this variable is TRUE    */
BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
INT display_period = 000;

/* maximum event size produced by this frontend */
INT max_event_size = 32 * 34000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
INT event_buffer_size = 2 * max_event_size + 10000;

/* VME base address */
int   v1730_handle[N_V1730];

int  linRun = 0;
int  done=0, stop_req=0;

/*-- Function declarations -----------------------------------------*/
INT frontend_init();
INT frontend_exit();
INT begin_of_run(INT run_number, char *error);
INT end_of_run(INT run_number, char *error);
INT pause_run(INT run_number, char *error);
INT resume_run(INT run_number, char *error);
INT frontend_loop();
int interrupt_configure(INT cmd, INT source, PTYPE adr);
INT read_trigger_event(char *pevent, INT off);
INT read_buffer_level(char *pevent, INT off);
INT read_temperature(char *pevent, INT off);



/*-- Equipment list ------------------------------------------------*/
#undef USE_INT
EQUIPMENT equipment[] = {

  { EQ_NAME,                 /* equipment name */
    {
      EQ_EVID, EQ_TRGMSK,     /* event ID, trigger mask */
      "SYSTEM",              /* event buffer */
      EQ_POLLED ,      /* equipment type */
      LAM_SOURCE(0, 0x0),     /* event source crate 0, all stations */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING,             /* read only when running */
      500,                    /* poll for 500ms */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",
    },
    read_trigger_event,       /* readout routine */
  },                                                                                
  {
    "V1730_Temperature",             /* equipment name */
    {
      100, 0x1000,            /* event ID, corrected with feIndex, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS |    /* read when running and on transitions */
      RO_ODB,                 /* and update ODB */
      1000,                   /* read every 1 sec */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* log history */
      "", "", ""
    },
    read_temperature,       /* readout routine */
  },
  {""}
};


/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

/********************************************************************/

/*-- Sequencer callback info  --------------------------------------*/
void seq_callback(INT hDB, INT hseq, void *info)
{
  KEY key;

	printf("odb ... Settings %x touched\n", hseq);
  for (int b=0;b<N_V1730;b++) {
    if (hseq == hSet[b]) {
      db_get_key(hDB, hseq, &key);
      printf("odb ... Settings %s touched\n", key.name);
    }
  }
}

/*-- Frontend Init -------------------------------------------------*/
INT frontend_init()
{
  int size, status;
  char set_str[80];

  // Suppress watchdog for PICe for now
  cm_set_watchdog_params(FALSE, 0);

  //  setbuf(stdout, NULL);
  //  setbuf(stderr, NULL);
  printf("begin of Init\n");
  /* Book Setting space */
  V1730_CONFIG_SETTINGS_STR(v1730_config_settings_str);

  sprintf(set_str, "/Equipment/feV1730Optic/Settings/V1730");
  status = db_create_record(hDB, 0, set_str, strcomb(v1730_config_settings_str));
  status = db_find_key (hDB, 0, set_str, &hSet[0]);
  if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);

  /* Enable hot-link on settings/ of the equipment */
  size = sizeof(V1730_CONFIG_SETTINGS);
  if ((status = db_open_record(hDB, hSet[0], &(tsvc[0]), size, MODE_READ, seq_callback, NULL)) != DB_SUCCESS)
    return status;

  printf("New V1730 object\n");
  myv1730Board = new v1730Board();
  printf("Connecting\n");
  myv1730Board->Connect();
  printf("Reset Board\n");
  myv1730Board->WriteReg(V1730_SW_RESET, 1);
  printf("Done reset.\n");





  char ss_fw_datatype[256]= {"Module "};
  char str[64];
  // Firmware version check (0x1n8C)
  // read each AMC firmware version
  // [31:16] Revision date Y/M/DD
  // [15:8] Firmware Revision (X)
  // [7:0] Firmware Revision (Y)
  // eg 0x760C0103 is 12th June 07, revision 1.3
  int addr = 0;
  DWORD version = 0;
  uint32_t prev_chan = 0;
  // Hardcode correct firmware verisons
  // Current release 3.4_0.11 Feb 2012
  // AMC FW: 0xd3280005, ROC FW: 0xd4110401
  //const uint32_t amc_fw_ver = 0xe7028802; // Update 27Nov2014
  const uint32_t amc_fw_ver = 0xe4140000; // Changed by TL (2015-05-12)
  const uint32_t roc_fw_ver = 0xe4280402; // Update 27Nov2014

  printf("Read AMC version number\n");
  // Somehow this doesn't work if we don't read the register first once
  myv1730Board->ReadReg(0x108c,&version);
  for(int iCh=0;iCh<16;iCh++) {
    addr = 0x108c | (iCh << 8);
    myv1730Board->ReadReg(addr,&version);
    //printf("%i %x %x \n",iCh, addr,version);
    if((iCh != 0) && (prev_chan != version)) {
      cm_msg(MERROR, "Initialize","Error Channels have different AMC Firmware ");
    }
    prev_chan = version;
  }
  if(version != amc_fw_ver) {
    cm_msg(MERROR,"Initialize","Incorrect AMC Firmware Version: 0x%08x", version);
  } else {
    sprintf(str, "AMC FW: 0x%x, ", version);
    strcat(ss_fw_datatype, str);
  }
  printf("AMC Firmware revision date: 20%i/%i/%i\n",
	 (version & 0xf0000000) >> 28,
	 (version & 0x0f000000) >> 24,
	 (version & 0x00ff0000) >> 16);
  
  // read ROC firmware revision
  // Format as above

  printf("Read ROC version number\n");
  myv1730Board->ReadReg(V1730_ROC_FPGA_FW_REV,&version);
  printf("Finished read ROC version number\n");
  switch (version) {
  case roc_fw_ver:
    sprintf(str, "ROC FW: 0x%x", version);
    strcat(ss_fw_datatype, str);
    break;
  default:
    //		cm_msg(MERROR,"Initialize","Incorrect ROC Firmware Version: 0x%08x", version);
    break;
  }
  printf("ROC Firmware revision date: 20%i/%i/%i\n",
	 (version & 0xf0000000) >> 28,
	 (version & 0x0f000000) >> 24,
	 (version & 0x00ff0000) >> 16);
  
  DWORD reg;
  myv1730Board->ReadReg(V1730_ACQUISITION_STATUS,&reg);
  sprintf(str, "Finished initialization, Acq Reg: 0x%x", reg); 
  strcat(ss_fw_datatype, str);
  cm_msg(MINFO, "Initialize", ss_fw_datatype);
  
  // Stop board
  myv1730Board->v1730_AcqCtl(V1730_RUN_STOP);


  //--------------- End of Init cm_msg debug ----------------
  set_equipment_status(equipment[0].name, "Initialized", "#00ff00");

	//exit(0);
  printf("end of Init\n");
  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/
INT frontend_exit()
{
  printf("End of exit\n");
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/
INT begin_of_run(INT run_number, char *error)
{

  char stastr[64];
  int status;
  //-------- V1730 (8ch 500Msps 124bits CAEN board) ----------------------------------
  printf("BOR\n");

  // Stop board
  myv1730Board->v1730_AcqCtl(V1730_RUN_STOP);

  /* read Triggger settings */
  int nInitOk=0;
  for (int module=0; module<N_V1730; module++) {
    int size = sizeof(V1730_CONFIG_SETTINGS);
    if ((status = db_get_record (hDB, hSet[module], &tsvc[module], &size, 0)) != DB_SUCCESS)
      return status;
    
    // load setup config is != 0
    if (tsvc[module].setup != 0) {
      cm_msg(MERROR, "AcqInit", "setup != 0 not supported yet...");
    }else{ 
      printf(" setup \n");
      // Reset ACQ Control
      myv1730Board->WriteReg(V1730_ACQUISITION_CONTROL, 0x00);
      // Acq mode 3:Reg 
      myv1730Board->v1730_AcqCtl(tsvc[module].acq_mode);


      myv1730Board->WriteReg(V1730_BUFFER_ORGANIZATION , tsvc[module].buffer_organization);
      // 0=disabled using BUFFER_ORGANIZATION
      myv1730Board->WriteReg(V1730_CUSTOM_SIZE, tsvc[module].custom_size);

      // channel enable 
      printf("Setting channel mask 0x%x\n",tsvc[module].ch_mask);
      myv1730Board->WriteReg(V1730_CHANNEL_EN_MASK, tsvc[module].ch_mask);
      DWORD mask;
      myv1730Board->ReadReg(V1730_CHANNEL_EN_MASK, &mask);
      printf("Channel mask readback 0x%x\n",mask);
      
      
      
      // Channel issuing trigger
      myv1730Board->WriteReg(V1730_TRIG_SRCE_EN_MASK, tsvc[module].trigger_source);
      //int readback_mask = v1730_RegisterRead(myvme, V1730_BASE[module], V1730_TRIG_SRCE_EN_MASK );
      //printf("Readback channel mask 0x%x\n",readback_mask);
      
      // channel on ECL OUT for trigger
      myv1730Board->WriteReg(V1730_FP_TRIGGER_OUT_EN_MASK, tsvc[module].trigger_output);

      myv1730Board->WriteReg(V1730_POST_TRIGGER_SETTING  , tsvc[module].post_trigger);

      myv1730Board->WriteReg(V1730_ALMOST_FULL_LEVEL , tsvc[module].almost_full);
      

      // Buff occupancy on MON out
      myv1730Board->WriteReg(V1730_MONITOR_MODE          , 0x3);  // buffer occupancy
      
      
      //      myv1730Board->WriteReg(V1730_VME_CONTROL, V1730_ALIGN64);
      
      // Check the channel configuration
      myv1730Board->WriteReg(V1730_CHANNEL_CONFIG,0x10010); 
      DWORD config;
      myv1730Board->ReadReg(V1730_CHANNEL_CONFIG, &config); 
      printf("Channel config = 0x%x\n",config);
    
  
      myv1730Board->WriteReg(0x8020, tsvc[module].sample_length); 
      DWORD nsamples;
      myv1730Board->ReadReg(0x8020, &nsamples); 
      printf("Nsamples = 0x%x\n",nsamples);
    
      // Setup Busy daisy chaining
      myv1730Board->WriteReg(V1730_FP_IO_CONTROL,   0x104); // 0x100:enable new config, 0x4:LVDS I/O[3..0] output
      myv1730Board->WriteReg(V1730_FP_LVDS_IO_CRTL, 0x022); // 0x20: I/O[7..4] input mode nBusy/Veto (4:nBusy input)
                                                                                    // 0x02: I/O[3..0] output mode nBusy/Veto (1:Busy)
    
      
      DWORD temp;
    
      // Do ADC calibration... 
      int addr;
      // This register poke does the ADC calibration
      myv1730Board->WriteReg(0X809C , 0);
      // Now we check to see when the calibration has finished.
      // by checking register 0x1n88.
      for (int i=0;i<16;i++) {
        addr = 0x1088 | (i << 8);
        myv1730Board->ReadReg(addr,&temp);
        printf("Channel (%i) Status: %x\n",i,temp);
        if((temp & 0x4) == 0x4){
          printf("waiting for ADC calibration to finish...\n");
          int j;
          for(j =0; j < 20; i++){
            sleep(1);
            printf("temp %x\n",temp);
            myv1730Board->ReadReg(addr,&temp);
            if((temp & 0x4) == 0x0){
              break;
            }
          }
          if(j < 19){
            printf("Took %i seconds to finish calibration\n",j+1);
          }else{
            printf("Calibration did not finish\n",j);
          }					
        }
      }
  

      // Set DAC value
      for (int i=0;i<16;i++) {
        addr = V1730_CHANNEL_DAC | (i << 8);
        myv1730Board->WriteReg(addr , tsvc[module].dac[i]);        
	myv1730Board->ReadReg(addr,&temp);
        printf("Board: %d DAC[%i] = %d %d 0x%x \n", module, i, temp,tsvc[module].dac[i],addr);
      }
      sleep(3);      
      for (int i=0;i<16;i++) {
        addr = V1730_CHANNEL_DAC | (i << 8);
	myv1730Board->ReadReg(addr,&temp);
        printf("Second DAC readback (%d) DAC[%i] = %d  \n", module, i, temp);
      }


      // Set channel gain
      for (int i=0;i<16;i++) {
        addr = V1730_CHANNEL_GAIN | (i << 8);
        myv1730Board->WriteReg(addr , tsvc[module].gain[i]);
	myv1730Board->ReadReg(addr,&temp);
        printf("Board: %d Gain[%i] = %d \n", module, i, temp);
      }
      
      // Set channel threshold
      for (int i=0;i<16;i++) {
        addr = V1730_CHANNEL_THRESHOLD | (i << 8);
        myv1730Board->WriteReg(addr , tsvc[module].threshold[i]);
	myv1730Board->ReadReg(addr,&temp);
        printf("Board: %d Threshold[%i] = %d \n", module, i, temp);
      }
      
      
      // SW clock sync
      myv1730Board->WriteReg(0x813C , 1);
      
      
      // Set Board ID in the data stream!
      myv1730Board->WriteReg(V1730_BOARD_ID, 6);
      printf(" setup \n");

      // print status
      //      v1730_Status(myvme, V1730_BASE[module]);
      
      // Abort if PLL not locked properly
      DWORD reg;
      myv1730Board->ReadReg(V1730_ACQUISITION_STATUS,&reg);
      cm_msg(MINFO, "AcqInit", "Acquisition Status : 0x%x", reg);
      if (!(reg & 0x80)) nInitOk++;
    } // setup != 0
  } // loop over module
  
  // Abort if PLL not locked properly
  if (nInitOk) {
    cm_msg(MINFO, "AcqInit", "PLL issues (%d)", nInitOk);
    return FE_ERR_HW;
  }
  
  // Start boards
  for (int module=0;module<N_V1730;module++) {
    if (V1730_BASE[module] < 0) continue;   // Skip unconnected board
    // Start run then wait for trigger
      myv1730Board->v1730_AcqCtl(V1730_RUN_START);
  }
 
  linRun = 1;
  //------ FINAL ACTIONS before BOR -----------
  printf("End of BOR\n");
  //sprintf(stastr,"GrpEn:0x%x", tsvc[0].group_mask); 
  set_equipment_status(equipment[0].name, "running", "#00FF00");                                                                        
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{

  printf("EOR\n");
  // Stop run 
  myv1730Board->v1730_AcqCtl(V1730_RUN_STOP);


  // Stop DAQ for seting up the parameters
  done = 0;
  stop_req = 0;
  linRun = 0;

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  linRun = 0;
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  linRun = 1;
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{

  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  char str[128];
  static DWORD evlimit;

  if (stop_req && done==0) {
    db_set_value(hDB,0,"/logger/channels/0/Settings/Event limit", &evlimit, sizeof(evlimit), 1, TID_DWORD); 
    if (cm_transition(TR_STOP, 0, str, sizeof(str), BM_NO_WAIT, FALSE) != CM_SUCCESS) {
      cm_msg(MERROR, "feodeap", "cannot stop run: %s", str);
    }
    linRun = 0;
    done = 1;
    cm_msg(MERROR, "feodeap","feodeap Stop requested");
  }
  return SUCCESS;
}

/*------------------------------------------------------------------*/
/********************************************************************\
  Readout routines for different events
\********************************************************************/
int Nloop, Ncount;

/*-- Trigger event routines ----------------------------------------*/
 INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  register int i;  // , mod=-1;
  register uint32_t lam;
  DWORD reg;

  for (i = 0; i < count; i++) {
    myv1730Board->ReadReg(V1730_EVENT_STORED,&reg);
    lam = reg;
    
    if (lam) {
      Nloop = i; Ncount = count;
      if (!test){
				//        printf("poll successfull, V1730_EVENT_STORED: 0x%x\n", lam);
        return lam;
      }
    }
    ss_sleep(1);
  }
  return 0;
}

/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, POINTER_T adr)
{
  switch (cmd) {
  case CMD_INTERRUPT_ENABLE:
    break;
  case CMD_INTERRUPT_DISABLE:
    break;
  case CMD_INTERRUPT_ATTACH:
    break;
  case CMD_INTERRUPT_DETACH:
    break;
  }
  return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/
int vf48_error = 0;
INT read_trigger_event(char *pevent, INT off)
{

  struct timeval start,stop;
  gettimeofday(&start,NULL);


  //  printf("entered read trig\n");
  uint32_t sn = SERIAL_NUMBER(pevent);
  
  // Create event header
  bk_init32(pevent);
	
  myv1730Board->FillEventBank(pevent);

  //primitive progress bar
  if (sn % 100 == 0){
    printf(".");
    gettimeofday(&stop,NULL);
    printf ("Elapsed during things_to_do...: %f\n",stop.tv_sec-start.tv_sec
	    + 0.000001*(stop.tv_usec-start.tv_usec));

  }

  return bk_size(pevent);
}



INT read_temperature(char *pevent, INT off) {

  // temporary testing.  Force a software trigger on each temperature read.
  //for(int i = 0; i < 1000; i++){
  //  usleep(50);
  //  myv1730Board->WriteReg(V1730_SW_TRIGGER,0x1); 
  // }


  bk_init32(pevent);

  DWORD *pdata;
  int addr;
  bk_create(pevent, "TEMP", TID_DWORD, (void **)&pdata);

  DWORD temp;
  std::cout << "Chan temperatures: ";
  for (int i=0;i<16;i++) {
    addr = V1730_CHANNEL_TEMPERATURE | (i << 8);
    myv1730Board->ReadReg(addr,&temp);
    std::cout << " " << temp;
    *pdata++ =  temp;
  }
  std::cout << std::endl;

  bk_close(pevent,pdata);


  return bk_size(pevent);
}

